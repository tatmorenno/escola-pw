var app = angular.module('app',['ngRoute']);
app.config(function($routeProvider, $locationProvider)
{
   $locationProvider.html5Mode(true);
   $routeProvider
   .when('/', {
      templateUrl : 'app/view/dashboardAluno.html',
      controller  : 'dashboardAlunoCtrl',
   })
   .when('/alunoFinanceiro', {
      templateUrl : 'app/view/financeiro.html',
      controller  : 'financeiroAlunoCtrl',
   })
   .when('/alunoBiblioteca', {
      templateUrl : 'app/view/bibliotecaAluno.html',
      controller  : 'bibliotecaAlunoCtrl',
   })
   .when('/alunoDisciplina', {
      templateUrl : 'app/view/alunoDisciplina.html',
      controller  : 'disciplinaAlunoCtrl',
   })
   .otherwise ({ redirectTo: '/' });
});

// INICIO MENU DO DASHBOARD
let menuAluno = [
	{
		nome: 'Início',
		icon: 'fas fa-fw fa-tachometer-alt',
		rota: '/'
	},
	{
		nome: 'Disciplinas',
		icon: 'fas fa-fw fa-cog',
		subitem: [{
			nome: 'Química',
			rota: '/alunoDisciplina'
		}]
	},
	{
		nome: 'Financeiro',
		icon: 'fas fa-fw fa-chart-area',
		rota: '/alunoFinanceiro'
	},
	{
		nome: 'Biblioteca',
		icon: 'fas fa-fw fa-book',
		rota: '/alunoBiblioteca'
	}
]; 
let menuProfessor   = []; 
let menuCoordenador = []; 
let menuBibliotecario = []; 
// FIM MENU DO DASHBOARD
